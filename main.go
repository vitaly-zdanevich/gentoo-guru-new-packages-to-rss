// How it works:
// git clone (yes full history from 2019)
// Iterate every commit that have metadata.xml
// Extract/build git commit message, DESCRIPTION, HOMEPAGE, date - and put to RSS struct
// Flush to file TODO upload to S3 or to the same repo?
// Average running time on my old laptop: ~5 minutes
// Every run will regenerate full RSS file - without append
// TODO upload to AWS Lambda
// TODO add tests

package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/storage/memory"
	"github.com/go-git/go-git/v5/utils/merkletrie"

	"github.com/baliw/moverss"
)

var (
	rss = moverss.ChannelFactory(
		"Gentoo GURU new packages",
		"https://github.com/gentoo/guru",
		"Repo of this RSS generator: https://gitlab.com/vitaly-zdanevich/gentoo-guru-new-packages-to-rss",
	)
	reTitl    = regexp.MustCompile(`(.+/.+): .+`) // TODO title can be invalid - take from the path?
	reDesc    = regexp.MustCompile(`(?s)DESCRIPTION="(.+?)"`)
	reHom     = regexp.MustCompile(`(?s)HOMEPAGE="\W*(http[s:\/\w\.-]+)`)
	reLicense = regexp.MustCompile(`LICENSE="(?s)(.+?)"`)
	reMe      = regexp.MustCompile(".+/.+/metadata.xml")
	reSign    = regexp.MustCompile("Signed-off-by: (.+)")
)

func main() {
	fmt.Println("Started")

	repo, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
		URL:           "https://anongit.gentoo.org/git/repo/proj/guru.git",
		ReferenceName: "master",
		SingleBranch:  true,
	})
	if err != nil {
		panic(err)
	}

	fmt.Println("Cloned")

	cIter, err := repo.Log(&git.LogOptions{
		Order: git.LogOrderCommitterTime,
		PathFilter: func(s string) bool {
			return reMe.MatchString(s)
		},
	})
	if err != nil {
		panic(err)
	}

	rss.SetPubDate(time.Now().UTC())

	err = cIter.ForEach(commitIterCb)
	if err != nil {
		panic(err)
	}

	err = os.WriteFile("public/gentoo-guru-new-packages.rss", rss.PublishIndent(), 0644)
	if err != nil {
		panic(err)
	}
}

func commitIterCb(c *object.Commit) error {
	parent, err := c.Parent(0)
	if err != nil {
		return nil
		// First commit without parrent
	}
	prev, err := parent.Tree()
	if err != nil {
		panic(err)
	}
	curr, err := c.Tree()
	if err != nil {
		panic(err)
	}

	changes, err := prev.Diff(curr)
	if err != nil {
		panic(err)
	}

	for _, change := range changes {
		_, to, err := change.Files()
		fmt.Println("to:", to)
		if err != nil {
			panic(err)
		}

		action, err := change.Action()
		if err != nil {
			panic(err)
		}

		if to != nil && to.Name == "metadata.xml" && action == merkletrie.Insert {
			desc, hom, license := filesIter(change.To.Tree.Files())

			title := reSign.ReplaceAllString(c.Message, "")

			pkgName := ""
			_pkgName := reTitl.FindStringSubmatch(title)
			if len(_pkgName) > 0 {
				pkgName = _pkgName[1]
			}

			rss.AddItem(&moverss.Item{
				Link: hom,
				Description: desc + // TODO Embed full ebuild here?
					"<br>" +
					"<br>" +
					"<a href='https://github.com/gentoo/guru/tree/master/" + pkgName + "'>Folder on GitHub</a>" +
					"<br>" +
					"<br>" +
					"<a href='https://github.com/gentoo/guru/commit/" + c.Hash.String() + "'>Commit</a>" +
					"<br>" +
					"<br>" +
					"LICENSE: " + license,
				Title:   title,
				PubDate: c.Author.When.Format(time.RFC822),
				Author:  c.Author.String(),
			})

			return nil

			// fmt.Print(".")
		}
	}

	return nil
}

func filesIter(fileIter *object.FileIter) (desc, hom, license string) {
	for {
		f, err := fileIter.Next()
		if err != nil {
			// No ebuild in this commit - user can push anything
			fmt.Println("+++++ err", err)
			return "", "", ""
		}

		fmt.Println("name:", f.Name)
		if strings.HasSuffix(f.Name, ".ebuild") {
			cont, err := f.Contents()
			if err != nil {
				panic(err)
			}
			descSl := reDesc.FindStringSubmatch(cont)
			if len(descSl) > 0 {
				desc = descSl[1]
			}

			homSl := reHom.FindStringSubmatch(cont)
			if len(homSl) > 0 {
				hom = homSl[1]
			}
			licenseSl := reLicense.FindStringSubmatch(cont)
			if len(licenseSl) > 0 {
				license = licenseSl[1]
				// fmt.Println("---", license)
			} else {
				// fmt.Println("==== No LICENSE:", cont)
			}
			return
		}
	}
}
